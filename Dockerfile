# Use an official Node.js runtime as a parent image
FROM node:16.16.0-alpine

# Set the working directory to /app
WORKDIR /app

# Copy the package.json and package-lock.json files to the working directory
COPY package*.json ./

# Install any needed packages specified in package.json
RUN npm install

# Copy the rest of the application code to the working directory
COPY . .

# Build the Angular application
RUN npm run build

# Use nginx as the server to serve the built application
FROM nginx:1.21.3-alpine

# Copy the built application from the previous stage
COPY --from=0 /app/dist /usr/share/nginx/html

# Expose port 80 to the Docker host
EXPOSE 80

# Start nginx when the container is launched
CMD ["nginx", "-g", "daemon off;"]