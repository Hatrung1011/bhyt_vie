import { Component, OnInit } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.scss'],
})
export class HomePageComponent implements OnInit {
  resdata: any = 0;
  project = { duration: 0 };

  formCaculate: FormGroup = this.formBuilder.group({
    salary: [null, [Validators.required]],
 
  });

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {}

  onCaculate() {
    if (this.formCaculate.invalid) {
      this.toastr.warning('Please enter full input', 'Warning', {
        timeOut: 3000,
      });
    } else {
      let param = new HttpParams()
        .set('salary', this.formCaculate.controls['salary'].value)
        .set('idConfig', this.project.duration);
      this.http
        .get('https://sqa-fha3.onrender.com/employees/bhxh/config', {
          params: param,
        })
        .subscribe((res) => {
          this.resdata = res;
        });
    }
  }
}
