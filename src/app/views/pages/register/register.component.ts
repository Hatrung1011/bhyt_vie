import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  resdata: any = {};

  formSignup: FormGroup = this.formBuilder.group({
    username: [null, [Validators.required]],
    password: [
      null,
      [
        Validators.required,
        Validators.pattern(
          '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
        ),
      ],
    ],
    confirmPassword: [
      null,
      [
        Validators.required,
        Validators.pattern(
          '^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$'
        ),
      ],
    ],
    name:[null,[Validators.required]]
  });

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {}

  onSignup() {
    debugger;
    if (this.formSignup.invalid) {
      this.toastr.warning(
        'Please enter full input or password must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters ',
        'Warning',
        {
          timeOut: 3000,
        }
      );
    } else {
      let signupForm = {
        username: this.formSignup.controls['username'].value,
        password: this.formSignup.controls['password'].value,
        confirmPass: this.formSignup.controls['confirmPassword'].value,
        name:this.formSignup.controls['name'].value,
      };
      this.http
        .post('https://sqa-fha3.onrender.com/auth/signup', signupForm)
        .subscribe((res) => {
          this.resdata = res;
          if (this.resdata.code == 1) {
            this.router.navigate(['login']);
            this.toastr.success(this.resdata.mess, 'Success', {
              timeOut: 3000,
            });
          } else {
            this.toastr.error(this.resdata.mess, 'Error', {
              timeOut: 3000,
            });
          }
        });
    }
  }
}
