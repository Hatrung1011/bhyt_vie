import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  resdata: any = {};

  formLogin: FormGroup = this.formBuilder.group({
    username: [null, [Validators.required]],
    password: [null, [Validators.required]],
  });
  
  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private router: Router
  ) { }

  ngOnInit() {
    this.buildForm();
  }

  buildForm() { }

  onLogin() {
    if(this.formLogin.invalid){
      this.toastr.warning('Please enter full input', 'Warning', {
        timeOut: 3000,
      });
    }else{
      let loginForm = {
        username: this.formLogin.controls['username'].value,
        password: this.formLogin.controls['password'].value,
      };
      this.http
        .post('https://sqa-fha3.onrender.com/auth/login', loginForm)
        .subscribe((res) => {
          this.resdata = res;
          if (this.resdata.code == 1) {
            this.router.navigate(['home']);
            this.toastr.success(this.resdata.mess, 'Success', {
              timeOut: 3000,
            });
          } else {
            this.toastr.error(this.resdata.mess, 'Error', {
              timeOut: 3000,
            });
          }
        });
    }
  }
}
