import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-config-base-salary',
  templateUrl: './config-base-salary.component.html',
  styleUrls: ['./config-base-salary.component.scss'],
})
export class ConfigBaseSalaryComponent implements OnInit {
  resdata: any = {};

  formConfig: FormGroup = this.formBuilder.group({
    baseSalary: [null, [Validators.required]],
  });

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private router: Router
  ) {}

  ngOnInit() {
    this.buildForm();
  }

  buildForm() {}

  onSave() {
    if (this.formConfig.invalid) {
      this.toastr.warning('Please enter full input', 'Warning', {
        timeOut: 3000,
      });
    } else {
      let configForm = {
        id: 1,
        baseSalary: this.formConfig.controls['baseSalary'].value,
      };
      this.http
        .post(
          'https://sqa-fha3.onrender.com/base_salary/change_base_salary',
          configForm
        )
        .subscribe((res) => {
          this.resdata = res;
          if (this.resdata.code == 1) {
            this.toastr.success(this.resdata.mess, 'Success', {
              timeOut: 3000,
            });
          } else {
            this.toastr.error(this.resdata.mess, 'Error', {
              timeOut: 3000,
            });
          }
        });
    }
  }
}
