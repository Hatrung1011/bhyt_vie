import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ConfigBaseSalaryComponent } from './config-base-salary.component';

describe('ConfigBaseSalaryComponent', () => {
  let component: ConfigBaseSalaryComponent;
  let fixture: ComponentFixture<ConfigBaseSalaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ConfigBaseSalaryComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ConfigBaseSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
