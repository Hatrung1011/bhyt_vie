import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { FloatingLabelsComponent } from './floating-labels/floating-labels.component';
import { FormControlsComponent } from './form-controls/form-controls.component';
import { InputGroupsComponent } from './input-groups/input-groups.component';
import { RangesComponent } from './ranges/ranges.component';
import { SelectComponent } from './select/select.component';
import { ChecksRadiosComponent } from './checks-radios/checks-radios.component';
import { LayoutComponent } from './layout/layout.component';
import { ValidationComponent } from './validation/validation.component';
import { ConfigBaseSalaryComponent } from './config-base-salary/config-base-salary.component';
import { HomeUserComponent } from './home-user/home-user.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Forms',
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'config-base-salary',
      },
      {
        path: 'config-base-salary',
        component: ConfigBaseSalaryComponent,
        data: {
          title: 'Cấu hình',
        },
      },
      {
        path: 'home-user',
        component: HomeUserComponent,
        data: {
          title: 'User',
        },
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class FormsRoutingModule {
}
