import { HttpClient } from '@angular/common/http';
import { Component,OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-home-user',
  templateUrl: './home-user.component.html',
  styleUrls: ['./home-user.component.scss']
})
export class HomeUserComponent {
  resdata: any = 0;
  project = { duration: 0 };
  money!:number
  formCaculate: FormGroup = this.formBuilder.group({
    salary: [null, [Validators.required]],
    name:[],
    
  });
  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    public toastr: ToastrService,
    private router: Router
  ) {}
  ngOnInit() {
  }
  onCaculate(){

  }
}
