import { INavData } from '@coreui/angular';

export const navItems: INavData[] = [
  {
    name: 'Quản lý',
    title: true,
  },
  {
    name: 'Quản lý ',
    url: '/forms',
    iconComponent: { name: 'cil-notes' },
    children: [
      {
        name: 'Cấu hình',
        url: '/forms/config-base-salary',
      },
      // {
      //   name: 'User',
      //   url: '/forms/home-user',
      // },
    ],
  },
];
